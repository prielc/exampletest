<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class AddruleController extends Controller
{
	public function actionOwnactivity()
	{ 
		$auth = Yii::$app->authManager; 
		$rule = new \app\rbac\OwnActivityRule;
		$auth->add($rule);
	}
}