<?php

use yii\db\Migration;

/**
 * Handles adding CategoryIId to table `user`.
 */
class m170716_101735_add_CategoryIId_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'CategoryId', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'CategoryId');
    }
}
